#!/usr/bin/env node
//#ver2017
//#needs to have hw setup /usr/bin/enable-pins.sh

/* Create a flow ramping up and down
 * FlowEmulator - output flow pulses based on irrigation input
 *  FlowOutputs P9_21
 * Boards
 * alpha BBG - custom board with 3FlowOutputs, and 10station inputs, + expand to another 10station inputs
 *      Board Hydr001 plugging into socket uses I2C2, & I2C2 extension
 *
 * proto BBG - custom wirewrap with 2FlowOutputs, and 2 MCP23017 on I2C1station inputs, +LCD on I2C1
 * Input Relay _49 _117 _115 _112 
 * Input NoFlow _5
 * Input MLB   _48
 *
 * Testing
 170621_1200 - testing on GregHw, 15inputs+MPV, two I2C addresses. 
 170619_1100 - run tests on 10 sequential channels
*/

/* Flow Provisioning Instructions
 The input relays are in multiples of 32 input relays, 
 the flow is added when the relays is activated
 Practically
 relayInputs on pcb Beta are ??
 relay input on pcb Alpha are 10 per MCP23017 - in array only 10 in first row, 10 in 2nd row 
 relay input on pcb Proto are 8 first 4 of first row, last 4 of 2nd row
flowOut1 tested
flowOut2 future  GE hw UN-tested
flowOut3 future  GE hw UN-tested
*/
//                1,  2,  3,  4,   5,  6,  7,  8,   9, 10, 11, 12,   13, 14, 15, 16 
var flowOut1 = [ 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 
                 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 0 ];
                 
var flowOut2 = [  1,  2,  3,  4,   5,  6,  7,  8,   9, 10, 11, 12,   13, 14, 15, 16, 
                 17, 18, 19, 20,  21, 22, 23, 24,  25, 26, 27, 28,   29, 30, 31, 32 ];
                 
var flowOut3 = [  1,  2,  3,  4,   5,  6,  7,  8,   9, 10, 11, 12,   13, 14, 15, 16, 
                 17, 18, 19, 20,  21, 22, 23, 24,  25, 26, 27, 28,   29, 30, 31, 32 ];
                 
//var stationInputActState =0; //0 or 1: internal input state that is active 

var b = require('bonescript');
var kelly_i2c = require('i2c'); //npm install i2c https://github.com/kelly/node-i2c
var sleep = require('sleep');
var dateFormat = require('dateformat');

var poll_mcp_sec = 2; //How ofterm the MCP23017 serializers are polled, flowOut generated
var poll_count =0; //Simple incrementing number of polls
 pulse_ms = 0.12; /* global pulse_ms */ // Current position, about middle

/* The hw has 4 output PWM - this maps to 3 ePWM subsystems
  Future: 4th output use Timer
*/
var flowMeter1 = 'P9_21';//gd ehrp0boutputs  #'P9_22';//error ehrp0A with ehrp0B
var flowMetersUsed=1;
if (flowMetersUsed>1 ) { 
  var flowMeter2 = 'P9_16';//gd ehrp1Boutputs verified in hw
  if (flowMetersUsed>2 ) { 
    var flowMeter3 = 'P8_13';//gd ehrp2Boutputs verified in hw
  }
}

//These are test freq for initialization - may be eventually removed
var freq1_hz = 1;  // Start frequency flowMeter1 - target 0
var freq2_hz = 2;  // Start frequency flowMeter2 - target 0
var freq3_hz = 3;  // Start frequency flowMeter3 - target 0

// enums = ??? require("./enums.js");

var board_beta=2;   //GregWilson board
var board_alpha=1;  //nh barebones
var board_proto=0;  //nh wirewrap

var board_me = board_beta;

//var btnState = [false,false,false,false];      // State of pushbutton


/////////************* 
// Read flowOptions and populate flowOutputX
//b.readTextFile(''); 
// from controller learned flow database

 zero=0, hex=16;
 i2cloc_port = '/dev/i2c-2';// local YxMCP23017 expansion, LCD oled 128x64
//var i2crem_port = '/dev/i2c-2'; //  remote YxMCP23017 expansion, Eeprom

/* Presence of MCPs on two I2C busses*/
 i2cNumMax=1; /* global i2cNumMax */ //range 1or2 - number of I2c busses supported.
//var i2c_lcd_addr =0x7c;  // 0 1 1 1 1 0 SA0 R/W#
//mcp23017 8possible Addr 0x20..0x27 per I2C bus - On BBG could be I2C1 or I2C2
  mcpNumMax=2; //per I2c bus 1:8 - Assumed same per bus - mapped on mcpX_act
  mcpNumStart=0; //Start from log 0..7
  mcpBitEnd=15; //Number of bits on mcp23017 16bits to read

//Tested for mcp addr  0x20 and 0x21
//Expect to have i2c1 up to 5 mcp23017 
//Expect to have i2c2 up to 5 mcp23017
 mcp0_addr = 0x20; //All boards support basic
//var mcp2_addr = 0x21; 

/*mcpx_act - indicates if the MCP23017 at that address is present 
 * - 8 possible addr per I2C
 * Initially set as present true, and then if doesn't respond marks as false
 * Currently requires logic to poll each I2C
*/
var mcp0_act= [true,true,true,true, true,true,true,true];
//var mcp1_act= [false,false,false,false, false,false,false,false];
  
if (board_proto==board_me) { 
  //var mcp2_addr = 0x24;
    stationInputActState=0; /* global stationInputActState */ //What state on station input us 
} else if (board_alpha==board_me) {
    //has I2c-1 and -2 and only one mcp 0x20
  //var i2cloc_port = '/dev/i2c-1';
   //var mcpNumMax=1; //range1:8 Assumed per bus
  //var mcp2_addr = 0x0; //don't use yet
  stationInputActState=1; //What state on station input us 
} else if ( board_beta==board_me) {
  //var i2cloc_port = '/dev/i2c-2';// only I2C2
  //var mcp2_addr = 0x24;  
   stationInputActState=1; //What state on station input us 
} else {
  console.log('Err: board_me not defined=' +board_me);
  return;
}

//console.log('I2c defined: 0x' +mcp0_addr.toString(hex) +' 0x' +mcp2_addr.toString(hex) .toString(hex));


/* //From https://stackoverflow.com/questions/7545641/javascript-multidimensional-array
Array.matrix = function(numrows, numcols, initial) {
    var arr = [];
    for (var i = 0; i < numrows; ++i) {
        var columns = [];
        for (var j = 0; j < numcols; ++j) {
            columns[j] = initial;
        }
        arr[i] = columns;
    }
    return arr;
}
var mcp_act1 = Array.matrix(2,8,false);
*/
var locMcp0 = new kelly_i2c(mcp0_addr+0, {device : i2cloc_port});
var locMcp1 = new kelly_i2c(mcp0_addr+1, {device : i2cloc_port}); 
//var locMcp[2..7] = new kelly_i2c(mcp0_addr+1, {device : i2cloc_port}); 
//kelly_i2c doesn't seem to support init using different I2cbusses

//the mcp23017 B&Aregs are read into the following 16 bit array. b15-8(Binputs) b7-0(Ainputs)
//Init value represents OFF for all 16 stations
//var mcpRead = Array.matrix(2,8,0); // [I2c#][mcp16bitIndex] 16bit station values
var mcpRead = [0x0, 0x0, 0x0, 0x0,    0x0, 0x0, 0x0, 0x0 ];
var timer_poll;
console.log('Hit ^C to stop. Poll station inputs over I2c every ~' +poll_mcp_sec +'Secs. Output Pulse width '+ pulse_ms +'uS.');

//******************************************************************************

mcp_poll('init'); //First time through, then on timer_poll

//Setup the flowMeter outputs 
b.pinMode(flowMeter1, b.ANALOG_OUTPUT, 6, 0, 0, doFlowMeter1);
if (flowMetersUsed>1 ) {
  b.pinMode(flowMeter2, b.ANALOG_OUTPUT, 6, 0, 0, doFlowMeter2);
  if (flowMetersUsed>2) {
    b.pinMode(flowMeter3, b.ANALOG_OUTPUT, 6, 0, 0, doFlowMeter3);
    
    //Fut:
   //b.pinMode("P9_49", b.DIGITAL_INPUT, 6, 0, 0, NULL);
  }
}
update_pwm_hz(1,freq1_hz);  //Init - first time through
if (flowMetersUsed>1 ) {
  update_pwm_hz(2,freq2_hz);  //Init - first time through
  if (flowMetersUsed>2 ) {
    update_pwm_hz(3,freq3_hz);  //Init - first time through
  }
}

function doFlowMeter1(x) {
    if(x.err) {
        console.log('do1:x.err = ' + x.err);
        return;
    }
    timer_poll = setInterval(sweep1, poll_mcp_sec*993); //magic number 
}

function doFlowMeter2(x) {
    if(x.err) {
        console.log('do2:x.err = ' + x.err);
        return;
    }
   //use sweep1: timer = setInterval(sweep2, ms);
}

function doFlowMeter3(x) {
    if(x.err) {
        console.log('do3:x.err = ' + x.err);
        return;
    }
    //use sweep1. timer = setInterval(sweep3, ms);
}

function sweep1() {
    mcp_poll(''); //only on this sweep
}

function mcp_poll(msg_dbg) 
{
    //var timeNow = dateFormat(new Date();,'HH:MM:ss.l');
    poll_count++;
    //console.log(timeNow+' ['+poll_count+']'+msg_dbg +' mcp0='+mcp0_act[0]+' mcp1=' +mcp1_act[0]);

    mcpNmLoc1=1;
	if (true == mcp0_act[mcpNmLoc1])  {
	   //console.log('mcp0_act['+mcpNmLoc1+'] poll');
  	  //Set mcp addr to GPIOA 0x12 or INTFA 0x0E
	  locMcp1.writeByte(0x12,function (err) { if (err) { console.log('mcp1wr:'+err.message);  }  });
	  locMcp1.read(2, function(err,rdData) {
	 	if (err) {
			console.log('mcp1rd:'+err);
			mcp0_act[mcpNmLoc1]=false;
		} else {
		   onReadMcp(mcpNmLoc1,rdData);
		}
	  });
	}

	mcpNmLoc0=0;	
	/* Called last so that it runs update_pwm_all() */
	if (true == mcp0_act[mcpNmLoc0]) {
      //console.log( 'mcp0_act['+mcpNmLoc0+'] poll');
	  //Set mcp addr to GPIOA 0x12 or INTFA 0x0E
	  locMcp0.writeByte(0x12,function (err) { if (err) { console.log('mcp0wr:'+err.message);  }  });
	  locMcp0.read(2, function(err,rdData) {
	 	if (err) {
			console.log('mcp0rd:'+err);
			mcp0_act[mcpNmLoc0]=false;
		} else {
		   onReadMcp(mcpNmLoc0,rdData); //This does work of recording data
		   update_pwm_all(); //Output flow
		}
	  });
	} else {
	    console.log("mcp0_act false, error no flow Output generated");
	}
}

function onReadMcp(mcpNmRd,rdValue) {
 	mcpRead[mcpNmRd] =rdValue[0] | (rdValue[1]<<8);
	//console.log('on mcpRead['+mcpNmRd+ ']=0x' +mcpRead[mcpNmRd].toString(hex) );
}

function update_pwm_all() {
    update_pwm_hz(1,freq1_hz);
    //update_pwm_hz(2,freq2_hz);
    //update_pwm_hz(3,freq3_hz);
    //update_pwm_hz(4,freq4_hz); hw needs integrating
}

function update_pwm_hz(flownum,freq_hz) {
    var dutyCycle = 0.5;
    var flowOut_PulsesSec=freq_hz;
    var flowMeter=flowMeter1;
    var mcpNum=mcpNumStart;//Start beginining of MCPs

    var stnMask, stnAct,stnLp,mcpLp,i2cLp;
        
    if (1==flownum) {
        flowMeter=flowMeter1;
        for (i2cLp=0;i2cLp<i2cNumMax;i2cLp++) {
            for (mcpLp=0;mcpLp<mcpNumMax;mcpLp++) {
                //console.log('flowout mcpRead['+i2cLp+'][' +mcpLp+ ']=0x' +mcpRead[i2cLp][mcpLp].toString(hex)+' 16stations');  
                for (stnLp=0;stnLp<mcpBitEnd;stnLp++) {
                    stnMask= (1<<stnLp);
        
                    if (1==stationInputActState) {
                       stnAct = stnMask & mcpRead[mcpLp];                
                    } else {
                       stnAct =!(stnMask & mcpRead[mcpLp]);                   
                    }
                    if (stnAct) {
                        //console.log('flowout[' +stnMask.toString(hex)+ ']=' +flowOut1[stnLp+(mcpLp*16)]+'stn/mcp'+stnLp+'/'+mcpLp);   
                        flowOut_PulsesSec +=flowOut1[stnLp+(mcpLp*16)];
                    }
                }
            }
        }
    dutyCycle = pulse_ms/1000*flowOut_PulsesSec;
    b.analogWrite(flowMeter, dutyCycle, flowOut_PulsesSec);
     var timeNow = dateFormat(new Date(),'HH:MM:ss');
    console.log(timeNow+'['+poll_count+'] flowM'+flownum + '= ' + flowOut_PulsesSec + " GPM  StationInput[High..Low]=0x" +mcpRead[mcpNum+1].toString(hex)+" " +mcpRead[mcpNum].toString(hex));
//    console.log(timeNow+': flowM'+flownum + '= ' + flowOut_PulsesSec + " station[0]=" +mcpRead[mcpNum+1].toString(hex)+" " +mcpRead[mcpNum].toString(hex)+"[1]=" +mcpRead[mcpNum+1].toString(hex)+" " +mcpRead[mcpNum].toString(hex));
    } else if (2==flownum) {
        flowMeter=flowMeter2;
        //TODO setup flow2 output
    } else if (3==flownum) {
        flowMeter=flowMeter3;
        //TODO setup flow3 outputs
    } 
    else {
      console.log('flownum invalid = ' + flownum);   
    }
}

//On Exit CTRL-C
process.on('SIGINT', function() {
    console.log('Ending: turning Pulses off');
    clearInterval(timer);             // Stop the timer
    b.analogWrite(flowMeter1, 0, freq);    // Set baseline off
});